
$(document).ready(function () {
    $('.menuContainer').load('menu.html');
    $('.footContainer').load('footer.html');

    $(".busquedaAvisos").on("click", function(){
        $(".avisosFiltrados").empty();
        var criterioBusquedaTipoReporte = $(".busquedaTipoReporte option:selected").val();
        var criterioBusquedaRaza = $(".busquedaRaza").val();
        var criterioBusquedaAnimal = $(".busquedaAnimal").val();
        var resultados = 0;

        if(criterioBusquedaRaza != ""){
            criterioBusquedaRaza = criterioBusquedaRaza.toLowerCase();
        }

        if(criterioBusquedaAnimal != ""){
            criterioBusquedaAnimal = criterioBusquedaAnimal.toLowerCase();
         }

        for(var i=0; i<avisos.length;i++){
            if(criterioBusquedaTipoReporte == avisos[i].tipoReporte || criterioBusquedaAnimal == avisos[i].perfil.animal.toLowerCase() || criterioBusquedaRaza == avisos[i].perfil.raza.toLowerCase()){
                $(".avisosFiltrados").append("<div class='col-sm-6 col-md-4 col-lg-3 mt-4' style='margin-bottom:100px;'>" + 
                                                "<div class='card'>" + 
                                                    "<img class='card-img-top' src='../images/mascotas/" + avisos[i].perfil.foto + "'>" +
                                                    "<div class='card-block'>" + 
                                                        "<h4 class='card-title mt-3'>" + avisos[i].perfil.nombre + ", " + avisos[i].tipoReporte + "</h4>" + 
                                                        "<div class='card-text'>" + 
                                                            "<p>" + avisos[i].perfil.descripcion + "</p>" + 
                                                            "<ul class='list-group list-group-flush datosAviso-" + i + "'>" + 
                                                                "<li class='list-group-item'>Tipo de reporte: " + avisos[i].tipoReporte + "</li>" + 
                                                                "<li class='list-group-item'>Animal: " + avisos[i].perfil.animal + "</li>" + 
                                                                "<li class='list-group-item'>Nombre: " + avisos[i].perfil.nombre + "</li>" + 
                                                                "<li class='list-group-item'>Raza: " + avisos[i].perfil.raza + "</li>" +  
                                                                "<li class='list-group-item'>Edad: " + avisos[i].perfil.edad + "</li>" + 
                                                                "<li class='list-group-item'>Tel&eacute;fono: " + avisos[i].contacto.telefono + "</li>" + 
                                                                "<li class='list-group-item'>Email: " + avisos[i].contacto.email + "</li>" + 
                                                            "</ul>" +
                                                        "</div>" + 
                                                    "</div>" +
                                                    "<div class='card-footer'>" + 
                                                        "<small>Fecha de publicaci&oacute;n: " + avisos[i].fechaPublicacion + "</small>" +
                                                    "</div>" +
                                                "</div>" +
                                            "</div>"
                );
                if(avisos[i].tipoReporte == "perdido" || avisos[i].tipoReporte == "encontrado"){
                    $(".datosAviso-" + i).append( "<li class='list-group-item'>Direcci&oacute;n: " + avisos[i].perfil.direccion + "</li>" +  
                                                    "<li class='list-group-item'>Fecha de suceso: " + avisos[i].perfil.fechaSuceso + "</li>" 
                    );
                }else{
                    $(".datosAviso-" + i).append("<li class='list-group-item'>Cuidados: " + avisos[i].perfil.cuidados + "</li>"
                    );
                }
                resultados++;
            }
        }

        if(resultados == 0){
            $(".avisosFiltrados").html("<p style='margin-left:70px'>No se encontraron resultados para la b&uacute;squeda de avisos.</p>");
        }
    });

    $(".busquedaAvisosPublicados").on("change", function(){
        var busquedaAvisos = $(".busquedaAvisosPublicados option:selected").val();
        if(busquedaAvisos == "tipoReporte"){
            $(".busquedaTipoReporte").show();
            $(".busquedaRaza").hide();
            $(".busquedaAnimal").hide();
            $(".busquedaRaza").val("");
            $(".busquedaAnimal").val("");
        }else if(busquedaAvisos == "raza"){
            $(".busquedaTipoReporte").hide();
            $(".busquedaRaza").show();
            $(".busquedaAnimal").hide();
            $(".busquedaTipoReporte").val("");
            $(".busquedaAnimal").val("");
        }else if(busquedaAvisos == "animal"){
            $(".busquedaTipoReporte").hide();
            $(".busquedaRaza").hide();
            $(".busquedaAnimal").show();
            $(".busquedaTipoReporte").val("");
            $(".busquedaRaza").val("");
        }else{
            $(".busquedaTipoReporte").hide("");
            $(".busquedaRaza").hide("");
            $(".busquedaAnimal").hide("");
            $(".busquedaTipoReporte").val("");
            $(".busquedaRaza").val("");
            $(".busquedaAnimal").val("");
        }
    });

   
});

