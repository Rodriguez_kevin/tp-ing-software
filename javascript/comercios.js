var map;
var marcadores = [];
$(document).ready(function () {
    $('.menuContainer').load('menu.html');
    $('.footContainer').load('footer.html');

    $(".busquedaComerciosFiltrados").on("click", function(){
        map.remove();
        mostrarMapa();
        $(".comerciosFiltrados").empty();
        $(".comerciosEncontrados").empty();
        $(".descripcionComercio").empty();
        var busquedaLocalidad = $(".busquedaLocalidad option:selected").val();
        var busquedaNombreComercio = $(".busquedaNombreComercio").val();
        var busquedaTipoServicio = $(".busquedaTipoServicio").val();
        var resultados = 0;

        if(busquedaNombreComercio != ""){
            busquedaNombreComercio = busquedaNombreComercio.toLowerCase();
        }

        if(busquedaTipoServicio != ""){
            busquedaTipoServicio = busquedaTipoServicio.toLowerCase();
        }

        for(var i=0; i<comercios.length;i++){
            if(busquedaLocalidad == comercios[i].ubicacion.localidad.toLowerCase() || busquedaNombreComercio == comercios[i].perfil.nombre.toLowerCase() || busquedaTipoServicio == comercios[i].perfil.tipoServicio.toLowerCase()){
                L.marker(comercios[i].ubicacion.coordenadas).bindTooltip(comercios[i].perfil.nombre, {permanent: true, direction: 'right'}).addTo(map);
                $(".comerciosEncontrados").append("<li><a href='#' onmouseover='irAComercio(event)' class='comercioEncontrado' data-coordenadax=" + comercios[i].ubicacion.coordenadas[0] + " data-coordenaday=" + comercios[i].ubicacion.coordenadas[1] + ">" + comercios[i].perfil.nombre + "</a></li>");
                $(".descripcionComercio").append("<div class='col-md-12'><div class='col-md-3'><h1>" + comercios[i].perfil.nombre + "</h1><img src='../images/comercios/" + comercios[i].perfil.foto + "'></div><div class='col-md-6' style='margin-top:60px'><p>" + comercios[i].perfil.descripcion + "</p><ul><li><p>Rubro: " + comercios[i].perfil.tipoServicio + "</p></li><li><p>Horario de Atenci&oacute;n: " + comercios[i].perfil.horario + "</p></li><li><p>Tel&eacute;fono: " + comercios[i].contacto.telefono + "</p></li><li><p>Email: " + comercios[i].contacto.email + "</p></li><li><p>Direcci&oacute;n: " + nombreLocalidad(comercios[i].ubicacion.localidad) + ", " + comercios[i].ubicacion.calleAltura + "</p></li></ul></div></div>");
                resultados++;
            }
        }

        if(resultados == 0){
            $(".comerciosFiltrados").html("<p style='margin-left:70px'>No se encontraron resultados para la b&uacute;squeda de comercios.</p>");
        }else{
            $(".comerciosFiltrados").html("");
        }
    });

    $(".busquedaComercios").on("change", function(){
        var busquedaComerciosDisponibles = $("#busquedaComercios option:selected").val();
        if(busquedaComerciosDisponibles == "localidad"){
            $(".busquedaLocalidad").show();
            $(".busquedaNombreComercio").hide();
            $(".busquedaTipoServicio").hide();
            $(".busquedaNombreComercio").val("");
            $(".busquedaTipoServicio").val("");
        }else if(busquedaComerciosDisponibles == "nombre"){
            $(".busquedaLocalidad").hide();
            $(".busquedaNombreComercio").show();
            $(".busquedaTipoServicio").hide();
            $(".busquedaLocalidad").val("");
            $(".busquedaTipoServicio").val("");
        }else if(busquedaComerciosDisponibles == "tipoServicio"){
            $(".busquedaLocalidad").hide();
            $(".busquedaNombreComercio").hide();
            $(".busquedaTipoServicio").show();
            $(".busquedaLocalidad").val("");
            $(".busquedaNombreComercio").val("");
        }else{
            $(".busquedaLocalidad").hide();
            $(".busquedaNombreComercio").hide();
            $(".busquedaTipoServicio").hide();
            $(".busquedaLocalidad").val("");
            $(".busquedaNombreComercio").val("");
            $(".busquedaTipoServicio").val("");
        }
    });
});

function mostrarMapa() {
    var ungsLocation = [-34.5221554, -58.7000067];
    map = L.map('mapid').setView(ungsLocation, 15);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
}

function nombreLocalidad(localidad){
    if(localidad == 1){
        return "Bella Vista";
    }

    if(localidad == 2){
        return "Muñiz";
    }

    if(localidad == 3){
        return "San Miguel";
    }

    if(localidad == 4){
        return "Polvorines";
    }
}

function irAComercio(event){
    var coordenadaX = $(event.target).data("coordenadax");
    var coordenadaY = $(event.target).data("coordenaday");

    map.flyTo([parseFloat(coordenadaX), parseFloat(coordenadaY)], 15);
}

function mostrarDatosComercio(){
    $(".descripcionComercio").show();
}



