$(document).ready(function(){
    $('.menuContainer').load('menu.html');
    $('.footContainer').load('footer.html');

    $("#personaButton").click(function(){
        $(".datosPersona").show();
        $(".datosOrganizacion").hide();
        $("#registrarse").show();
        $(".tipoRegistro").val("persona");
    });

    $("#organizacionButton").click(function(){
        $(".datosPersona").show();
        $(".datosOrganizacion").show();
        $("#registrarse").show();
        $(".tipoRegistro").val("organizacion");
    });
    
    $("#registrarse").click(function(){
        var nombrePersona = $("#nombrePersona").val();
        var telefonoPersona = $("#telefonoPersona").val();
        var emailPersona = $("#emailPersona").val();

        if($(".tipoRegistro").val() == "persona"){
            if( nombrePersona != '' && telefonoPersona != '' && emailPersona != ''){
                confirmarRegistro();
            }  
        }else{
            var nombreOrganizacion = $("#nombreOrganizacion").val();
            var telefonoOrganizacion = $("#telefonoOrganizacion").val();
            var emailOrganizacion = $("#emailOrganizacion").val();
            if( nombrePersona != '' && telefonoPersona != '' && emailPersona != '' && nombreOrganizacion != '' && telefonoOrganizacion != '' && emailOrganizacion != ''){
                confirmarRegistro();
            }
        }
       
    });
});

function confirmarRegistro() {
    $("#registroConfirmacion").modal();
    
    $(".btn-primary").click(function(){
        $("#nombrePersona").val("");
        $("#telefonoPersona").val("");
        $("#emailPersona").val("");
        $("#nombreOrganizacion").val("");
        $("#telefonoOrganizacion").val("");
        $("#emailOrganizacion").val("");
        window.location.href = "login.html";
    });
   
}
