var comercio1 = {};
comercio1.contacto = {};
comercio1.contacto.telefono = "46665555";
comercio1.contacto.email = "mitreveterinaria@outlook.com";
comercio1.perfil = {};
comercio1.perfil.nombre = "Mitre";
comercio1.perfil.foto = "Mitre.jpg";
comercio1.perfil.horario = "10 a 20hs"
comercio1.perfil.tipoServicio = "veterinaria";
comercio1.perfil.descripcion= "Desde 1997 Mitre brinda la mejor atenci&oacute;n veterinaria para tu mascota. Para ello, seleccionamos los mejores profesionales veterinarios, quienes contin&uacute;an en capacitaci&oacute;n y formaci&oacute;n constante, para darle la más c&aacute;lida y profesional atención a tu mascota.";
comercio1.ubicacion = {};
comercio1.ubicacion.localidad = "3";
comercio1.ubicacion.calleAltura = "Av Balbin 2000";
comercio1.ubicacion.coordenadas = [-34.548165, -58.718432];
comercio1.destacado = false;

var comercio2 = {};
comercio2.contacto = {};
comercio2.contacto.telefono = "5415165465";
comercio2.contacto.email = "columnasvet@gmail.com";
comercio2.perfil = {};
comercio2.perfil.nombre = "Las Columnas";
comercio2.perfil.foto = "Las columnas.jpg";
comercio2.perfil.tipoServicio = "veterinaria";
comercio2.perfil.horario = "9 a 13hs - 17 a 20hs";
comercio2.perfil.descripcion= "Las Columnas es una empresa familiar con más de 60 años en el rubro veterinario. Comenz&oacute; siendo la m&aacute;s famosa peluquer&iacute;a canina para convertirse en la empresa m&aacute;s prometedora del rubro.";
comercio2.ubicacion = {};
comercio2.ubicacion.localidad = "1";
comercio2.ubicacion.calleAltura = "Senador Moron 1000";
comercio2.ubicacion.coordenadas = [-34.560702, -58.679419];
comercio2.destacado = false;

var comercio3 = {};
comercio3.contacto = {};
comercio3.contacto.telefono = "5743256487";
comercio3.contacto.email = "tiendamascotas@yahoo.com.ar";
comercio3.perfil = {};
comercio3.perfil.nombre = "Tienda de Mascotas";
comercio3.perfil.foto = "Tienda_Mascotas.jpg";
comercio3.perfil.tipoServicio = "venta";
comercio3.perfil.horario = "9 a 18hs";
comercio3.perfil.descripcion= "Tienda de Mascotas ha sido desarrollada por una empresa de gran experiencia en el mundo del ecommerce y de los negocios online, junto con los mejores mayoristas y fabricantes de comida para perros, asesorado per veterninarios expertos del sector de los animales.";
comercio3.ubicacion = {};
comercio3.ubicacion.localidad = "3";
comercio3.ubicacion.calleAltura = "Roca 600";
comercio3.ubicacion.coordenadas = [-34.534527, -58.710753];
comercio3.destacado = true;

var comercio4 = {};
comercio4.contacto = {};
comercio4.contacto.telefono = "21542378";
comercio4.contacto.email = "asambleaventas@gmail.com";
comercio4.perfil = {};
comercio4.perfil.nombre = "Asamblea";
comercio4.perfil.foto = "Asamblea.jpg";
comercio4.perfil.tipoServicio = "venta";
comercio4.perfil.horario = "9 a 20hs";
comercio4.perfil.descripcion= "Somos una empresa familiar que se inici&oacute; en el año 1990 en la ciudad de Buenos Aires, especializada en juguetes y accesorios para perros y gatos. Nuestros productos son de fabricaci&oacute;n propia con diseños exclusivos y originales hechos de manera artesanal.";
comercio4.ubicacion = {};
comercio4.ubicacion.localidad = "3";
comercio4.ubicacion.calleAltura = "Zuviria 2024";
comercio4.ubicacion.coordenadas = [-34.543295, -58.722585];
comercio4.destacado = false;

var comercio5 = {};
comercio5.contacto = {};
comercio5.contacto.telefono = "324576834";
comercio5.contacto.email = "homeroso01@gmail.com";
comercio5.perfil = {};
comercio5.perfil.nombre = "Homeroso";
comercio5.perfil.foto = "Homeroso.jpg";
comercio5.perfil.tipoServicio = "peluqueria";
comercio5.perfil.horario = "10 a 21hs";
comercio5.perfil.descripcion= "La mejor atenci&oacute;n para tu mascota, baños, cortes de raza y cortes de pelo originales, corte de uñas, limpieza de oí&iacute;dos, coloraci&oacute;n y mucho mas!";
comercio5.ubicacion = {};
comercio5.ubicacion.localidad = "3";
comercio5.ubicacion.calleAltura = "España 1040";
comercio5.ubicacion.coordenadas = [-34.538870, -58.713539];
comercio5.destacado = false;

var comercio6 = {};
comercio6.contacto = {};
comercio6.contacto.telefono = "54654654165";
comercio6.contacto.email = "perrikos123@gmail.com";
comercio6.perfil = {};
comercio6.perfil.nombre = "Perrikos";
comercio6.perfil.foto = "Perrikos.jpg";
comercio6.perfil.tipoServicio = "peluqueria";
comercio6.perfil.horario = "9 a 19hs";
comercio6.perfil.descripcion= "Somos una empresa familiar dedicada a la estética y el bienestar de su mascota. Ofreciendo la mas alta calidad en productos y servicio al alcance de su mano.";
comercio6.ubicacion = {};
comercio6.ubicacion.localidad = "2";
comercio6.ubicacion.calleAltura = "Paunero 500";
comercio6.ubicacion.coordenadas = [-34.549633, -58.700034];
comercio6.destacado = true;

var comercios = [comercio1, comercio2, comercio3, comercio4, comercio5, comercio6];