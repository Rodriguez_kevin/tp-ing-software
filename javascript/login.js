$(document).ready(function(){
    $('.menuContainer').load('menu.html');
    $('.footContainer').load('footer.html');
    
    $("#login").click(function(){
        var usuario = $("#usuario").val();
        var password = $("#password").val();

        if( usuario != '' && password != ''){
            loguear();
            window.location.href = "index.html";
        }
    });
});

function loguear() {
    var nombre = $("#usuario").val();
    localStorage.setItem('nombreUsuario', nombre);
    $("#usuario").val("");
    $("#password").val("");

}
