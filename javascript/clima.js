$(document).ready(function () {
    $('.menuContainer').load('menu.html');
    $('.footContainer').load('footer.html');
    climaOficial();
    alertasOficial();
});

function climaOficial() {
    var urlNac= "https://ws.smn.gob.ar"
    var clima='/map_items/weather'

    fetch(urlNac+clima)
        .catch(climaRespaldo)
        .then(datosClima => datosClima.json())
        .then(datosClima =>
        {
            for (let i=0;i<datosClima.length;i++){
                if(datosClima[i].name == "Campo de Mayo"){
                    dibujarClima(datosClima[i].weather);
                }
            }
        })
}

function alertasOficial() {
    var url = "https://ws.smn.gob.ar";
    var urlAlertas= '/alerts/type/AL';

    fetch(url + urlAlertas)
        .catch(alertasRespaldo)
        .then(data => data.json() )
        .then(data =>{
            for(let alerta of Object.keys(data)){
                dibujarAlerta(data[alerta]);
            }
        })
}

function dibujarClima(dataClima) {
    var imagen = imagenClima(dataClima.description);
    $('.panelClima').append(
        '<div class="panel panel-primary" align="center">'+
            '<div class="panel-heading"><b>Clima Hoy</b></div>'+
            '<div class="panel-body" >'+
            "<img src='../images/clima/" + imagen +"' width='200' height='200'>" +
                '<div><b><h3>'+ dataClima.description +'</h3></b></div>'+
                '<div><b>Temperatura: </b>'+ dataClima.temp +'°c</div>'+
                '<div><b>Humedad: </b>'+dataClima.humidity+'%</div>'+
                '<div><b>Vientos del sector: </b>'+dataClima.wing_deg+'</div>'+
            '</div>' +
        '</div>'
    )
}

function dibujarAlerta(data) {
    var zonas=zonasAlertas(data.zones);
    $('.panelesAlertas').append(
        '<div class="panel panel-danger">'+
            '<div class="panel-heading"><b>Alerta '+data.title+'</b></div>'+
            '<div class="panel-body">'+
                '<div><b>Fecha: </b>'+data.date+'</div>'+
                '<br>'+
                '<div><b>Descripcion: </b>' + data.description+'</div>'+
                '<br>'+
                '<div><b>Zonas afectadas: </b>'+zonas+'</div>'+
            '</div>'+
        '</div>'+
        '<br>'
    );
}

function zonasAlertas(zonas) {
    var zona ='';
    for (let zon of Object.keys(zonas)) {
        zona+=zonas[zon] + ', ';
    }
    return zona;
}

function imagenClima(descripcion) {

    if(descripcion.includes("Despejado")){
        return 'soleado.png'
    }
    else if(descripcion.includes("Tormenta")){
        return 'tormenta.png'
    }
    else if(descripcion.includes("llovizna") || descripcion.includes("lluvia") ){
        return 'lluvia.jpg'
    }
    else if(descripcion.includes("Algo") || descripcion.includes("Parcialmente") ){
        return 'algoNublado.png'
    }
    else if(descripcion.includes("Nublado")|| descripcion.includes("Cubierto")){
        return 'nublado.jpg'
    }

}

function alertasRespaldo() {
    var heroku = "https://weatherservices.herokuapp.com/api";
    var urlAlertas= '/alerts/byDay/';
    var dias=['0','1','2','3'];

    for (let dia of dias){
        fetch(heroku + urlAlertas + dia)
            .then(data => data.json() )
            .then(data =>{
                var alertas = data.alerts;
                for(let alerta of Object.keys(alertas)){
                    dibujarAlerta(alertas[alerta]);
                }
            })
    }
}

function climaRespaldo() {
    var heroku = "https://weatherservices.herokuapp.com/api";
    var urlClima = '/weather/';
    fetch(heroku + urlClima )
        .then(data => data.json() )
        .then(data => data['items'])
        .then(data =>
        {
            var dataClima = data[0].weather;
            dibujarClima(dataClima);

        })
}